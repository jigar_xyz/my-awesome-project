# My Awesome Project
This repository aims to demonstrate fully end-to-end [Golang](https://golang.org/) projects.

## Projects
- [Building a GraphQL API in Go using gqlgen](./go-graphql-gqlgen.md)
