package graph

import "gitlab.com/jigar_xyz/my-awesome-project/pkg/todo"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	ToDo todo.ToDo
}
